import SwiftUI

struct ContentView: View {
    @EnvironmentObject var viewModel : ContentViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            TextField("HintForename", text: $viewModel.forename)
            TextField("HintSurname", text: $viewModel.surname)
            HStack {
                Text("LabelForename")
                Text(viewModel.forename)
            }
            HStack {
                Text("LabelSurname")
                Text(viewModel.surname)
            }
            Text(viewModel.greeting)
        }
        .padding(.horizontal)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(ContentViewModel())
    }
}
