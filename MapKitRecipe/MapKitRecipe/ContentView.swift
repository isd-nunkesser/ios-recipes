//
//  ContentView.swift
//  MapKitRecipe
//
//  Created by Prof. Dr. Nunkesser, Robin on 21.01.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftUI
import MapKit

struct ContentView: View {
    
    @State var pins: [MapPin] = [
        MapPin(coordinate: CLLocationCoordinate2D(latitude: 51.509865,
                                                  longitude: -0.118092),
               title: "London",
               subtitle: "England",
               action: { print("Clicked!") } )
    ]
    @State var selectedPin: MapPin?
    
    var body: some View {
        VStack {
            MapView(coordinate: CLLocationCoordinate2D(
                latitude: 51.492668,
                longitude: 7.451767))
                .frame(width: 300, height: 300)
            Map(pins: $pins, selectedPin: $selectedPin)
                .frame(width: 300, height: 300)
            if selectedPin != nil {
                Text(verbatim: "Welcome to \(selectedPin?.title ?? "???")!")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
