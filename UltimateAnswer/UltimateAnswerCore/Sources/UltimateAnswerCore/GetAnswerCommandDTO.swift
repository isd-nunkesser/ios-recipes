import Foundation

public class GetAnswerCommandDTO {
    var question : String
    
    public init(question: String) {
        self.question = question
    }
}
