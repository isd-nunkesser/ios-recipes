import Foundation

class Configuration: NSObject {
    private let kPlistName = "Configuration"
    private let kSetting1Key = "setting1"
    let setting1: String
    
    override init() {
        if let path = Bundle.main.path(forResource: kPlistName,
                                       ofType: "plist"),
            let plist = NSDictionary(contentsOfFile: path),
            let setting1 = plist[kSetting1Key] as? String
        {
            self.setting1 = setting1
            super.init()
        } else {
            fatalError("Could not load plist into object properties")
        }
    }
}
