import Foundation

public class GetPostCommandDTO {
    var id : Int
    
    public init(id: Int) {
        self.id = id
    }
}

