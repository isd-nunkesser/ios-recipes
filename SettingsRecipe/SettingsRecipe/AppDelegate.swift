import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
        
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions:
            [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        registerDefaultsFromSettingsBundle()
        return true
    }
    
    func registerDefaultsFromSettingsBundle()
    {
        let plists = ["Root.plist"]
        for plist in plists {
            let settingsUrl = Bundle.main.url(forResource: "Settings",
                                              withExtension: "bundle")!
                .appendingPathComponent(plist)
            let settingsPlist = NSDictionary(contentsOf:settingsUrl)!
            let prefs = settingsPlist["PreferenceSpecifiers"] as! [NSDictionary]
            
            var defaultsToRegister = Dictionary<String, Any>()
            
            for preference in prefs {
                guard let key = preference["Key"] as? String else {
                    debugPrint("Key not found for \(preference["Title"] ?? "")")
                    continue
                }
                defaultsToRegister[key] = preference["DefaultValue"]
            }
            UserDefaults.standard.register(defaults: defaultsToRegister)
        }
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication,
            configurationForConnecting connectingSceneSession: UISceneSession,
            options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration",
                                    sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication,
                     didDiscardSceneSessions sceneSessions: Set<UISceneSession>)
    { }
    
    
}

