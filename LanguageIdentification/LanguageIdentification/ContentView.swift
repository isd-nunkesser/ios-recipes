//
//  ContentView.swift
//  LanguageIdentification
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.10.20.
//

import SwiftUI
import LanguageIdentificationInfrastructure

struct ContentView: View {
    
    @State var text = "This is a test, mein Freund."
    @State var showResult = false
    @State var result = ""
    
    let recognizer = LanguageRecognizerAdapter()
    
    var body: some View {
        VStack {
            TextEditor(text: $text)
            HStack {
                Button("Identify Language", action: identifyLanguage)
                Button("Identify Possible Languages", action: identifyPossibleLanguages)
            }
        }.padding()
        .alert(isPresented: $showResult, content: {
            Alert(title: Text("Result"),message: Text(result),dismissButton: .cancel(Text("OK")))
        })
    }
    
    func identifyLanguage() {
        recognizer.identifyLanguage(text: text, completion: {
            switch $0 {
            case let .success(response):
                result = response
                showResult = true
            case let .failure(error):
                debugPrint(error.localizedDescription)
            }
        })
    }
    
    func identifyPossibleLanguages() {
        recognizer.identifyPossibleLanguages(text: text, completion: {
            switch $0 {
            case let .success(response):
                result = response.description
                showResult = true
            case let .failure(error):
                debugPrint(error.localizedDescription)
            }
        })
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
