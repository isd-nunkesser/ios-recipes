//
//  LanguageIdentificationApp.swift
//  LanguageIdentification
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.10.20.
//

import SwiftUI

@main
struct LanguageIdentificationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
