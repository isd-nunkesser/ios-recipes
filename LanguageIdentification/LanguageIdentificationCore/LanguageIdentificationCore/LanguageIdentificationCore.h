//
//  LanguageIdentificationCore.h
//  LanguageIdentificationCore
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.10.20.
//

#import <Foundation/Foundation.h>

//! Project version number for LanguageIdentificationCore.
FOUNDATION_EXPORT double LanguageIdentificationCoreVersionNumber;

//! Project version string for LanguageIdentificationCore.
FOUNDATION_EXPORT const unsigned char LanguageIdentificationCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LanguageIdentificationCore/PublicHeader.h>


