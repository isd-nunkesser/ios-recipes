//
//  LanguageRecognizer.swift
//  LanguageIdentificationCore
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.10.20.
//

import Foundation

public protocol LanguageRecognizer {
    
    func identifyLanguage(text: String, completion: @escaping (Result<String, Error>) -> Void)
    
    func identifyPossibleLanguages(text: String, completion: @escaping (Result<[(language: String, confidence: Double)], Error>) -> Void)
    
}
