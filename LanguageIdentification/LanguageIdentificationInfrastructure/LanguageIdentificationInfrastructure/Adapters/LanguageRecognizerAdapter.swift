//
//  LanguageRecognizerAdapter.swift
//  LanguageIdentificationInfrastructure
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.10.20.
//

import Foundation
import LanguageIdentificationCore
import NaturalLanguage

public class LanguageRecognizerAdapter : LanguageRecognizer {
    
    enum LanguageRecognizerError: Error {
        case noLanguageRecognized
    }
    
    public init() {
        
    }

    
    public func identifyLanguage(text: String, completion: @escaping (Result<String, Error>) -> Void) {
        let recognizer = NLLanguageRecognizer()
        recognizer.processString(text)
        if let language = recognizer.dominantLanguage {
            completion(Result.success(language.rawValue))
        } else {
            completion(Result.failure(LanguageRecognizerError.noLanguageRecognized))
        }
    }
    
    public func identifyPossibleLanguages(text: String, completion: @escaping (Result<[(language: String, confidence: Double)], Error>) -> Void) {
        let recognizer = NLLanguageRecognizer()
        recognizer.processString(text)
        // Generate up to two language hypotheses.
        let hypotheses = recognizer.languageHypotheses(withMaximum: 2)
        let mappedHypotheses = hypotheses.map { (language: $0.key.rawValue,confidence: $0.value)  }
        completion(Result.success(mappedHypotheses))
    }
    
}
