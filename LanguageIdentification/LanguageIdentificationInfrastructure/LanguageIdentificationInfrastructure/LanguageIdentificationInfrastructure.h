//
//  LanguageIdentificationInfrastructure.h
//  LanguageIdentificationInfrastructure
//
//  Created by Prof. Dr. Nunkesser, Robin on 23.10.20.
//

#import <Foundation/Foundation.h>

//! Project version number for LanguageIdentificationInfrastructure.
FOUNDATION_EXPORT double LanguageIdentificationInfrastructureVersionNumber;

//! Project version string for LanguageIdentificationInfrastructure.
FOUNDATION_EXPORT const unsigned char LanguageIdentificationInfrastructureVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LanguageIdentificationInfrastructure/PublicHeader.h>


