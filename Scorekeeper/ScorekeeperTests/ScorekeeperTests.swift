//
//  ScorekeeperTests.swift
//  ScorekeeperTests
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.02.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import XCTest
@testable import Scorekeeper

class ScorekeeperTests: XCTestCase {

    // Todo: Only score increase of 1, 2, 3 is allowed
    // Todo: Make score private

    func testInitialScore() {
        let teamA = Team(name: "A")
        XCTAssertEqual(teamA.score, 0)
    }
    
    func testAddTwoToScore() {
        var teamA = Team(name: "A")
        teamA.increaseScore(amount: Score.Two)
        XCTAssertEqual(teamA.score, 2)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
