//
//  ContentViewModel.swift
//  Scorekeeper
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.02.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

class ContentViewModel : ObservableObject {
    @Published var scoreA : String = "000"
    @Published var scoreB : String = "000"
    
    var teamA = Team(name: "A")
    var teamB = Team(name: "B")
    
    func increaseScoreAByOne() {
        teamA.increaseScore(amount: Score.One)
        scoreA = "\(teamA.score)"
    }
    
}
