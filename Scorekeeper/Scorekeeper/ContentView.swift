//
//  ContentView.swift
//  Scorekeeper
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.02.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var teamSelected = false
    @EnvironmentObject var viewModel : ContentViewModel
    
    var body: some View {
        VStack {
            HStack {
                Text(viewModel.scoreA)
                Text(":")
                Text(viewModel.scoreB)
            }
            HStack {
                Button(action: {
                    self.teamSelected.toggle()
                }) {
                    Text("Team A")
                }
                Button(action: {
                    self.teamSelected.toggle()
                }) {
                    Text("Team B")
                }
            }
            HStack {
                Button(action: {
                    self.viewModel.increaseScoreAByOne()
                }) {
                    Text("1")
                }.disabled(!teamSelected)
                Button(action: {}) {
                    Text("2")
                }.disabled(!teamSelected)
                Button(action: {}) {
                    Text("3")
                }.disabled(!teamSelected)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(ContentViewModel())
    }
}
