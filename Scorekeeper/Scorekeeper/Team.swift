//
//  Team.swift
//  Scorekeeper
//
//  Created by Prof. Dr. Nunkesser, Robin on 14.02.20.
//  Copyright © 2020 Hochschule Hamm-Lippstadt. All rights reserved.
//

import Foundation

struct Team {
    private(set) var score : Int = 0
    var name : String
    
    init(name : String) {
        self.name = name
    }
    
    mutating func increaseScore(amount : Score) {
        score = score + amount.rawValue
    }
}

enum Score : Int {
    case One = 1, Two, Three
}
