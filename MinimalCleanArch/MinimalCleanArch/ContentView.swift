import SwiftUI
import BasicCleanArch

struct ContentView: View, Displayer {
    typealias ViewModelType = String
    
    @State var result = ""
    let interactor = MinimalInteractor(presenter: MinimalPresenter())
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: startInteraction) {
                Text("Start")
            }
            Text(result)
        }
    }
    
    func startInteraction() {
         interactor.execute(request: nil, displayer: self)
    }
    
    func display(success: String, resultCode: Int) {
        result = success
    }
    
    func display(failure: Error) {
        debugPrint(failure.localizedDescription)
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
