import Foundation
import BasicCleanArch

class MinimalPresenter : Presenter {
    typealias Model = Int
    typealias ViewModel = String
    
    func present(model: Int) -> String {
        return model.description
    }
}
