import Foundation
import BasicCleanArch

class MinimalInteractor : UseCase {
    
    typealias DisplayerType = ContentView
    typealias PresenterType = MinimalPresenter
    typealias RequestType = Void?
    
    var presenter: MinimalPresenter
    
    required init(presenter : MinimalPresenter) {
        self.presenter = presenter
    }
    
    func execute(request: Void?, displayer: ContentView, resultCode: Int) {
        let model = 42
        let viewModel = presenter.present(model: model)
        displayer.display(success: viewModel, resultCode: resultCode)
    }
}
